#! /bin/bash

./gradlew clean build -x test

JAR_FILE=$(ls build/libs/ | grep "^finance-gateway")

docker build . --build-arg jar=build/libs/$JAR_FILE -t ezzefiohez/finance-gateway
docker push ezzefiohez/finance-gateway

echo " ######## BUILD GATEWAY DONE ######## "

curl  -X POST http://146.59.195.214:9000/api/webhooks/0308130b-2f5c-4c1e-abf5-e3320fd29249
