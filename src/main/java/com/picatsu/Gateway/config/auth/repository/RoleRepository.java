package com.picatsu.Gateway.config.auth.repository;


import com.picatsu.Gateway.config.auth.models.ERole;
import com.picatsu.Gateway.config.auth.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;


public interface RoleRepository extends MongoRepository<Role, String> {
  Optional<Role> findByName(ERole name);

  @Override
  <S extends Role> S save(S s);
}
