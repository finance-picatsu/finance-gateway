package com.picatsu.Gateway.config.auth.controllers;


import com.picatsu.Gateway.config.auth.models.ERole;
import com.picatsu.Gateway.config.auth.models.Role;
import com.picatsu.Gateway.config.auth.models.User;
import com.picatsu.Gateway.config.auth.payload.request.LoginRequest;
import com.picatsu.Gateway.config.auth.payload.request.SignupRequest;
import com.picatsu.Gateway.config.auth.payload.response.JwtResponse;
import com.picatsu.Gateway.config.auth.payload.response.MessageResponse;
import com.picatsu.Gateway.config.auth.repository.RoleRepository;
import com.picatsu.Gateway.config.auth.repository.UserRepository;
import com.picatsu.Gateway.config.auth.security.jwt.JwtProvider;
import com.picatsu.Gateway.config.auth.security.services.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController("AuthController")
@RequestMapping("/api/v1/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@GetMapping("/getInfos/{id}")// TODO REMOVE THIS IN PROD
	@Operation(summary = "Retrieve user infos from DB, temporary for debug")
	public ImmutablePair<String, String>  getUserInfos(@PathVariable("id") String id, HttpServletRequest request)
			throws Exception {

		return  new ImmutablePair<>(
				userRepository.findById(id).get().getEmail(),
				userRepository.findById(id).get().getUsername()  );
	}


	@GetMapping("/getRoles")// TODO REMOVE THIS IN PROD
	@Operation(summary = "Retrieve all user roles from DB, temporary for debug")
	public ERole[] getRoles(HttpServletRequest request) throws Exception {
		return ERole.values() ;
	}

	@PostMapping("/login")
	@Operation(summary = "Retrieve jwt for given user")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtProvider.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt,
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}

	@PostMapping("/create")
	@Operation(summary = "signin user to access ressource")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(),
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRoles();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "ROLE_ADMIN":
						Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(adminRole);

						break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}
