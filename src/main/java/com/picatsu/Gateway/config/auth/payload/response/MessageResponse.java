package com.picatsu.Gateway.config.auth.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageResponse {
	private String message;

}
