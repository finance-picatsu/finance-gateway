package com.picatsu.Gateway.config.zullConfig;


import com.picatsu.Gateway.config.zullConfig.filters.ErrorFilter;
import com.picatsu.Gateway.config.zullConfig.filters.PostFilter;
import com.picatsu.Gateway.config.zullConfig.filters.PreFilter;
import com.picatsu.Gateway.config.zullConfig.filters.RouteFilter;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableZuulProxy
@Configuration
public class FilterConfig {

    @Bean
    public PreFilter preFilter() {
        return new PreFilter();
    }

    @Bean
    public PostFilter postFilter() {
        return new PostFilter();
    }

    @Bean
    public ErrorFilter errorFilter() {
        return new ErrorFilter();
    }

    @Bean
    public RouteFilter routeFilter() {
        return new RouteFilter();
    }
}

