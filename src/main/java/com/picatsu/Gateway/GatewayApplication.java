package com.picatsu.Gateway;

import com.picatsu.Gateway.config.auth.models.ERole;
import com.picatsu.Gateway.config.auth.models.Role;
import com.picatsu.Gateway.config.auth.repository.RoleRepository;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
@EnableAsync
@EnableCaching
@EnableZuulProxy
@EnableAdminServer

public class GatewayApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	@Autowired
	RoleRepository roleRepository;

	@Override
	public void run(String... args) {

		Role role = new Role();
		role.setId("1");
		role.setName(ERole.ROLE_USER);
		roleRepository.save(role);


		Role role4 = new Role();
		role4.setId("4");
		role4.setName(ERole.ROLE_ADMIN);
		roleRepository.save(role4);


	}



}
